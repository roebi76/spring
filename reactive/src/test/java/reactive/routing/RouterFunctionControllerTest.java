package reactive.routing;

import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

//@SpringBootTest
public class RouterFunctionControllerTest {

//	@Test
//	public void testHello() {
//		WebTestClient.bindToRouterFunction(new RouterFunctionConfig(personService))
//			.build());
//	}

	@Test
//	@Ignore
	public void shouldReturnPerson() throws JsonProcessingException {
		Person expectedPerson = new Person("b", 22);
		Flux<Person> personFlux = Flux.just(expectedPerson);

		PersonService personService = Mockito.mock(PersonService.class);
		when(personService.getPerson()).thenReturn(Mono.just(expectedPerson));

		WebTestClient testClient = WebTestClient.bindToRouterFunction(
			new RouterFunctionConfig(personService).helloRouterFunction())
			.build();

		testClient.get().uri("/person")
			.exchange()
			.expectStatus().isOk()
			.expectBody()
			.json(new ObjectMapper().writeValueAsString(expectedPerson));
	}
}
