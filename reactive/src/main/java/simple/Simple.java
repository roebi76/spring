package simple;

import java.util.stream.IntStream;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import reactor.core.publisher.Flux;

public class Simple {

	private static final class MySubscriber implements Subscriber<Integer> {
		Subscription subscription;

		@Override
		public void onSubscribe(Subscription s) {
			System.out.println("on Subscribe() wurde aufgerufen");
			System.out.println("ich requeste 10 Stück!!!");
			this.subscription = s;
			s.request(10);

		}

		@Override
		public void onNext(Integer i) {
			System.out.println("value: " + i);
			if (i == 10) {
				subscription.request(5);
			}
			if (i == 14) {
				System.out.println("request to cancel, please");
				subscription.cancel();
			}

		}

		@Override
		public void onError(Throwable t) {
			System.out.println("upsi");

		}

		@Override
		public void onComplete() {
			System.out.println("feddisch");

		}
	}

	public static void main(String[] args) {
		reactive();
	}

	private static void reactive() {
		Flux.fromStream(IntStream.rangeClosed(1, 20).mapToObj(i -> i))
			.map(n -> {
//				if (n == 7) {
//					throw new RuntimeException("extra geworfen");
//				}
				return n;
			})
			.log()
			.subscribe(new MySubscriber());
	}

}
