package reactive.routing;

import org.springframework.stereotype.Service;

import reactor.core.publisher.Mono;

@Service
public interface PersonService {
	Mono<Person> getPerson();

}
