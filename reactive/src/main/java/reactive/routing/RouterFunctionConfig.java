package reactive.routing;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;
import static reactor.core.publisher.Mono.just;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import reactor.core.publisher.Mono;

@Configuration
public class RouterFunctionConfig {

	private PersonService personService;

	public RouterFunctionConfig(PersonService personService) {
		this.personService = personService;
	}

	@Bean
	public RouterFunction<?> helloRouterFunction() {
		return route(GET("/hello"), request -> ok().body(just("Hello World!"), String.class))
			.andRoute(GET("/bye"), request -> ok().body(just("tschüssi"), String.class))
			.andRoute(GET("/person"), this::getPersonMono);
//			.andRoute(POST("/person2"), this::setPersonMono);
	}

	private Mono<ServerResponse> setPersonMono(ServerRequest request) {
		Person personReceived = request.bodyToMono(Person.class).block();
		Person personResponse = new Person(personReceived.getName() + "X", personReceived.getAge() * 2);
		return ok().body(Mono.just(personResponse), Person.class);
	}

	public Mono<ServerResponse> getPersonMono(ServerRequest request) {
		return ok().body(personService.getPerson(), Person.class);
	}

}