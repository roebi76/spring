package reactive.routing;

import org.springframework.stereotype.Component;

import reactor.core.publisher.Mono;

@Component
public class PersonServiceImpl implements PersonService {

	@Override
	public Mono<Person> getPerson() {
		return Mono.just(new Person("a", 10));
	}

}
