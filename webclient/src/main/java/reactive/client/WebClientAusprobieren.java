package reactive.client;

import java.util.Collections;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersSpec;

import reactor.core.publisher.Mono;

public class WebClientAusprobieren {

	public static void main(String... args) {
		WebClient client = createClient();

		String helloRetrieved = client.get()
			.uri("/hello")
			.retrieve()
			.bodyToMono(String.class)
			.block();
		System.out.println(helloRetrieved);

		String personRetrieved = client.get()
			.uri("/person")
			.retrieve()
			.bodyToMono(String.class)
			.block();
		System.out.println(personRetrieved);

//		Person personRetrieved = createPost(client)
//			.retrieve()
//			.bodyToMono(Person.class)
//			.block();
//		System.out.println(personRetrieved);
	}

	private static RequestHeadersSpec<?> createPost(WebClient client) {
		return client.post()
			.uri("/person2")
			.body(BodyInserters.fromPublisher(Mono.just(new Person("thename", 100)), Person.class));
	}

	private static RequestHeadersSpec<?> createGet(WebClient client) {
		return client.get()
			.uri("/hello");
	}

	private static WebClient createClient() {
		return WebClient
			.builder()
			.baseUrl("http://localhost:8080")
			.defaultCookie("cookieKey", "cookieValue")
			.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
			.defaultUriVariables(Collections.singletonMap("url", "http://localhost:8080"))
			.build();
	}

}
